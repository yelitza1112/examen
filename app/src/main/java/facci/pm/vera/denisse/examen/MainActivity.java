package facci.pm.vera.denisse.examen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Infracciones> infraccionesArrayList;
    private Adapter_Infracciones adapter_infracciones;
    private ImageView imageView;
    private TextView marca, modelo, anio;
    private static String URLJO = "http://10.32.28.214:3005/vehiculos/AAA111";
    private static String URLJA = "http://10.32.28.214:3005/vehiculos/infracciones/AAA111";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.RecyclerCarro);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        infraccionesArrayList = new ArrayList<>();
        adapter_infracciones = new Adapter_Infracciones(infraccionesArrayList);
        imageView = (ImageView)findViewById(R.id.ImgCarro);
        marca = (TextView)findViewById(R.id.MarcaC);
        modelo = (TextView)findViewById(R.id.Modelo);
        anio = (TextView)findViewById(R.id.Anio);

        DatosCarro();
        DatosInfracciones();
    }

    private void DatosInfracciones() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLJA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Infracciones infracciones = new Infracciones();
                        infracciones.setFecha(jsonObject.getString("fecha"));
                        infracciones.setHora(jsonObject.getString("hora"));
                        infracciones.setPagada(jsonObject.getString("pagada"));
                        infracciones.setValor(String.valueOf(jsonObject.getInt("valor")));
                        infraccionesArrayList.add(infracciones);
                    }
                    recyclerView.setAdapter(adapter_infracciones);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void DatosCarro() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLJO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    marca.setText(jsonObject.getString("marca"));
                    modelo.setText(jsonObject.getString("modelo"));
                    anio.setText(jsonObject.getString("anio_fabricacion"));
                    Picasso.get().load(jsonObject.getString("imagen")).into(imageView);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
