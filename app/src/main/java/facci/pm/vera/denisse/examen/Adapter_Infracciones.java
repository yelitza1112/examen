package facci.pm.vera.denisse.examen;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adapter_Infracciones extends RecyclerView.Adapter<Adapter_Infracciones.ViewH>{

    private ArrayList<Infracciones> modelo_recyclerArrayList;

    public Adapter_Infracciones(ArrayList<Infracciones> modelo_recyclerArrayList) {
        this.modelo_recyclerArrayList = modelo_recyclerArrayList;
    }

    @NonNull
    @Override
    public ViewH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewH(LayoutInflater.from(parent.getContext()).inflate(R.layout.vista_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewH holder, int position) {
        Infracciones infracciones = modelo_recyclerArrayList.get(position);
        holder.valor.setText(infracciones.getValor());
        holder.pagada.setText(infracciones.getPagada());
        holder.hora.setText(infracciones.getHora());
        holder.fecha.setText(infracciones.getFecha());
    }

    @Override
    public int getItemCount() {
        return modelo_recyclerArrayList.size();
    }

    public static class ViewH extends RecyclerView.ViewHolder{

        private TextView fecha, hora, valor, pagada;

        public ViewH(@NonNull View itemView) {
            super(itemView);
            fecha = (TextView)itemView.findViewById(R.id.Fecha);
            hora = (TextView)itemView.findViewById(R.id.Hora);
            valor = (TextView)itemView.findViewById(R.id.Valor);
            pagada = (TextView)itemView.findViewById(R.id.Pagada);
        }
    }
}
